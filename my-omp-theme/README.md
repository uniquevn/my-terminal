## References
- onehalf.minimal
    - https://ohmyposh.dev/docs/themes#onehalfminimal
    - https://github.com/JanDeDobbeleer/oh-my-posh/blob/main/themes/onehalf.minimal.omp.json
    - Good:
        - Simple, only highlight text, no block background
        - No need NerdFont
    - Bad:
        - In WSL Bash, cause delay after finishing a command. It may be cause of the git branch checking
- sim-web
    - https://ohmyposh.dev/docs/themes#sim-web
    - https://github.com/JanDeDobbeleer/oh-my-posh/blob/main/themes/sim-web.omp.json
    - Good:
        - Doesn't cause delay after executing command in Bash
        - Provide execution time of the last command

## Notes
- Fancy git branch status detection may look nice but it may cause delay on the prompt after each command
