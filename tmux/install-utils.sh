#!/usr/bin/env bash

# Copy the .tmux.conf
SCRIPT_DIR=$(dirname $0)
cp $SCRIPT_DIR/config/.tmux.conf ~/.tmux.conf
tmux source-file ~/.tmux.conf

# Install tmuxp which help save/load tmux session1
# https://github.com/tmux-python/tmuxp
pip install --user tmuxp