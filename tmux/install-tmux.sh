#!/usr/bin/env bash

# NOTE: This script is inspired by: https://github.com/bogdanvlviv/i-vagrant/blob/master/ubuntu/tmux/install-tmux.sh

TMP_TMUX_DIR='~/temp/tmux'
TMUX_VERSION='2.8'

# Install the needed tool to build the tmux
sudo apt update
sudo apt install -y automake
sudo apt install -y build-essential
sudo apt install -y pkg-config
sudo apt install -y libevent-dev
sudo apt install -y libncurses5-dev

# Clone the tmux git repo
rm -fr $TMP_TMUX_DIR
git clone https://github.com/tmux/tmux.git $TMP_TMUX_DIR
cd $TMP_TMUX_DIR
git checkout $TMUX_VERSION

# Remove the current tmux
sudo killall -9 tmux
sudo apt-get -y remove tmux

# Make and install the tmux
sh autogen.sh
./configure && make
sudo make install
cd -
